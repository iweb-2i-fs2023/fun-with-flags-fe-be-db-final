import express from 'express';
import { loadCountriesIfNeeded } from './backend/country/country.controller.js';
import { router as questionRouter } from './backend/question/question.routes.js';
import { router as answerRouter } from './backend/answer/answer.routes.js';
import mongoose from 'mongoose';

const app = express();

mongoose.connect('mongodb://localhost/quizOfFlags');

app.use(express.static('frontend'));

app.use(express.json());
app.use('/api/questions', questionRouter);
app.use('/api/answers', answerRouter);

mongoose.connection.once('open', async () => {
  console.log('Connected to MongoDB');
  await loadCountriesIfNeeded();
  console.log('Countries loaded');
  app.listen(3001, () => {
    console.log('Server listens to http://localhost:3001');
  });
});
