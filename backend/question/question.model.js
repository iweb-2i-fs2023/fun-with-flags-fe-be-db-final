import mongoose from 'mongoose';

const optionSchema = new mongoose.Schema({
  optionNr: {
    type: Number,
    required: true,
  },
  flagUrl: {
    type: String,
    required: true,
  },
});

const questionSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  options: {
    type: [optionSchema],
    required: true,
  },
  correctOptionNr: {
    type: Number,
    required: true,
  },
});

const model = mongoose.model('questions', questionSchema);

export { model };
