import { Router } from 'express';
import { getNextQuestion } from './question.controller.js';

const router = Router();

router.get('/next', getNextQuestion);

export { router };
