import { Router } from 'express';
import { createAndCheckAnswer } from './answer.controller.js';

const router = Router();

router.post('/', createAndCheckAnswer);

export { router };
