import mongoose from 'mongoose';

const answerSchema = new mongoose.Schema({
  questionId: {
    type: mongoose.ObjectId,
    required: true,
  },
  optionNrChosen: {
    type: Number,
    required: true,
  },
});

const model = mongoose.model('answers', answerSchema);

export { model };
