import { model as Answer } from './answer.model.js';
import { getQuestion } from '../question/question.controller.js';

async function createAndCheckAnswer(request, response) {
  const answerGiven = request.body;
  const optionNrChosen = parseInt(answerGiven.optionNrChosen);
  if (!optionNrChosen || optionNrChosen < 1 || optionNrChosen > 4) {
    return response.status(400).json({ message: 'answer wrongly formatted' });
  }
  const questionId = answerGiven.questionId;
  const answer = await getQuestion(questionId)
    .then(async (question) => {
      if (!question) {
        return Promise.reject(`No question with id ${questionId}`);
      }
      const answersSoFar = await Answer.find({ questionId });
      if (answersSoFar && answersSoFar.length >= 4) {
        return Promise.reject(
          `Too much answers for question with id ${questionId}`
        );
      }
      return question;
    })
    .then(async (question) => {
      // Wenn Antwort zu Frage passt, diese abspeichern
      await Answer.create(answerGiven);
      // Ermitteln ob Antwort korrekt war
      let isAnswerCorrect = false;
      if (question.correctOptionNr === optionNrChosen) {
        isAnswerCorrect = true;
      }
      return { isCorrect: isAnswerCorrect };
    })
    .catch((error) => {
      response.status(400);
      return { message: error };
    });
  response.json(answer);
}

export { createAndCheckAnswer };
