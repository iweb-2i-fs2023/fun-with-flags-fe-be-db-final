import mongoose from 'mongoose';

const countrySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  region: {
    type: String,
    required: true,
  },
  flagUrl: {
    type: String,
    required: true,
  },
});

const model = mongoose.model('countries', countrySchema);

export { model };
