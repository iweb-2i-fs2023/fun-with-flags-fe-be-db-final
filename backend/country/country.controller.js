import axios from 'axios';
import { model as Country } from './country.model.js';

async function loadCountriesIfNeeded() {
  const countries = await Country.find().exec();
  if (countries && countries.length > 0) {
    return;
  }
  return axios
    .get('https://restcountries.com/v3.1/all')
    .then(async (response) => {
      const countries = response.data;
      // Geladene Informationen einzelner Länder iterieren
      for (let i = 0; i < countries.length; i++) {
        const country = countries[i];
        // Objekt mit benötigten Informationen für Quiz anlegen ...
        const newCountry = {
          name: country.translations.deu.common,
          region: country.region,
          flagUrl: country.flags.svg,
        };
        await Country.create(newCountry);
      }
    });
}

async function getRandomCountries(nrOfCountries) {
  // Hilfsfunktion: Gibt zufällig die Indizes von vier Ländern
  // zurück
  function getRandomIndices(totalNrOfCountries) {
    let indices = [];
    while (indices.length !== 4) {
      let index = parseInt(Math.random() * totalNrOfCountries);
      if (indices.indexOf(index) === -1) {
        indices.push(index);
      }
    }
    return indices;
  }

  const countries = await Country.find().exec();
  const randomCountries = [];
  // Zufällig vier Länder auswählen
  const indices = getRandomIndices(countries.length);
  for (let i = 0; i < nrOfCountries; i++) {
    randomCountries.push(countries[indices[i]]);
  }
  return Promise.resolve(randomCountries);
}

export { loadCountriesIfNeeded, getRandomCountries };
