function getNextQuestion() {
  const request = axios.get('/api/questions/next');
  return request.then((response) => response.data);
}

function submitAnswer(answer) {
  const request = axios.post('/api/answers', answer);
  return request.then((response) => response.data);
}

export { getNextQuestion, submitAnswer };
